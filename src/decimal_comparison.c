#include "decimal.h"
#include "decimal__extra.h"

int is_less(decimal_t a, decimal_t b) {
  int res = 0;
  decimal_t tmp;
  if (!(is_empty(&a) && is_empty(&b))) {
    if (!compare_sign(a, b)) {
      if (a.bits_.sign == 1) res = 1;
    } else {
      if (a.bits_.exp != b.bits_.exp) {
        normalization(&a, &b);
      }
      if (a.bits_.sign) {
        tmp = a;
        a = b;
        b = tmp;
      }
      res = is_less_no_norm(a, b);
    }
  }
  return res;
}

int is_less_or_equal(decimal_t a, decimal_t b) {
  return (is_less(a, b) || is_equal(a, b));
}

int is_greater(decimal_t a, decimal_t b) {
  return !is_less_or_equal(a, b);
}

int is_greater_or_equal(decimal_t a, decimal_t b) {
  return (!is_less(a, b) || is_equal(a, b));
}

int is_equal(decimal_t a, decimal_t b) {
  int res = compare_sign(a, b);
  if (res) {
    if (a.bits_.exp != b.bits_.exp) {
      normalization(&a, &b);
    }
    res = compare_val(a, b);
  }
  return (is_empty(&a) && is_empty(&b)) ? 1 : res;
}

int is_not_equal(decimal_t a, decimal_t b) {
  return !is_equal(a, b);
}
