#!bin/bash

dotnet_v="$(dotnet --info | grep ".NETCore.App" | awk '{print $2}' | grep -o "^[0-9]*")"

if [ $dotnet_v -eq 7 ]; then
    dotnet run --project ./sharp_gen_check/sharp_gen_check/sharp_gen_check.csproj
  else
    if [ -d ~/goinfre ]
    then
        mkdir -p ~/goinfre/Docker/Data
        ln -s ~/goinfre/Docker ~/Library/Containers/com.docker.docker
    else
        echo start
    fi

    docker ps
    if [ $? -eq 1 ]
    then
        echo please RUN docker
    else
        docker rmi -f docker_sharp_gen_check
        docker rm docker_generator
        docker   build . -t docker_sharp_gen_check -f sharp_gen_check/sharp_gen_check/Dockerfile
        docker run --name docker_generator -v $(pwd)/tests:/app/tests -it  docker_sharp_gen_check
        docker rmi -f docker_sharp_gen_check
        docker rm docker_generator
    fi
fi
