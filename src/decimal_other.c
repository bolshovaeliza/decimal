#include "decimal.h"
#include "decimal__extra.h"

int truncate(decimal_t value, decimal_t *result) {
  for (; value.bits_.exp; value.bits_.exp--) {
    div_by_x(&value, 10);
  }
  equating_decimal(value, result);
  return 0;
}

int negate(decimal_t value, decimal_t *result) {
  for (int i = 0; i < 4; i++) result->bits[i] = value.bits[i];
  result->bits_.sign = !result->bits_.sign;
  return 0;
}

int decimal_floor(decimal_t value, decimal_t *result) {
  decimal_t temp = {0, 0, 0, 0};
  if (value.bits_.sign) {
    truncate(value, &temp);
    if (is_equal(temp, value)) {
      equating_decimal(temp, result);
    } else {
      sub(temp, (decimal_t){1, 0, 0, 0}, result);
    }
  } else {
    truncate(value, result);
  }
  return 0;
}

int decimal_round(decimal_t value, decimal_t *result) {
  round_struct br = {0};
  while (value.bits_.exp) {
    bank_round(&value, &br);
  }
  equating_decimal(value, result);
  return 0;
}
