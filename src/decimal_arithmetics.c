#include <math.h>
#include <stdio.h>

#include "decimal.h"
#include "decimal__extra.h"

int add(decimal_t value_1, decimal_t value_2, decimal_t *result) {
  round_struct round_data = normalization(&value_1, &value_2);
  result->bits_.exp = (value_1.bits_.exp >= value_2.bits_.exp)
                          ? value_1.bits_.exp
                          : value_2.bits_.exp;
  int res;
  if (value_1.bits_.sign == value_2.bits_.sign) {
    res = add_with_round(value_1, value_2, round_data, result);
    result->bits_.sign = value_1.bits_.sign;
    if (res && value_1.bits_.sign == 1) res = 2;
  } else {
    if (is_less_no_norm(value_2, value_1)) {
      res = subtraction(value_1, value_2, result);
      result->bits_.sign = value_1.bits_.sign;
    } else {
      res = subtraction(value_2, value_1, result);
      result->bits_.sign = value_2.bits_.sign;
    }
    subtraction_correction(result, &round_data);
  }
  return res;
}

int sub(decimal_t value_1, decimal_t value_2, decimal_t *result) {
  round_struct round_data = normalization(&value_1, &value_2);
  result->bits_.exp = (value_1.bits_.exp >= value_2.bits_.exp)
                          ? value_1.bits_.exp
                          : value_2.bits_.exp;
  int res;
  result->bits_.exp = value_1.bits_.exp;
  if (value_1.bits_.sign == value_2.bits_.sign) {
    if (is_less_no_norm(value_2, value_1)) {
      res = subtraction(value_1, value_2, result);
      result->bits_.sign = value_1.bits_.sign;
    } else {
      res = subtraction(value_2, value_1, result);
      result->bits_.sign = (value_1.bits_.sign == 0);
    }
    subtraction_correction(result, &round_data);
  } else {
    res = add_with_round(value_1, value_2, round_data, result);
    result->bits_.sign = value_1.bits_.sign;
    if (res && value_1.bits_.sign) res++;
  }
  if (is_empty(result) && value_1.bits_.sign && value_2.bits_.sign)
    result->bits_.sign = 1;
  return res;
}

int mul(decimal_t value_1, decimal_t value_2, decimal_t *result) {
  if (is_empty(&value_1) && is_empty(&value_2)) return 0;
  decimal_t hi, tmp, add, lo = hi = (decimal_t){0, 0, 0, 0};
  int res = 0, sign = value_1.bits_.sign ^ value_2.bits_.sign;
  int num_v1 = num_len(value_1, 10), num_v2 = num_len(value_2, 10);
  *result = (decimal_t){0, 0, 0, 0};
  mul_hi_lo(value_1, value_2, &hi, &lo);
  int num_lo = num_len(lo, 10), num_hi = num_len(hi, 10);
  if (num_v1 + num_v2 >= 29 && !is_empty(&hi)) {
    int exp = 29 - num_lo - num_hi + value_1.bits_.exp + value_2.bits_.exp -
              unscale(&hi, &lo, 29 - num_lo - num_hi);
    if (exp < 0)
      res = sign ? 2 : 1;
    else {
      if (exp > 28) {
        unscale(&hi, &lo, 28 - exp);
        exp = 28;
      };
      *result = lo;
      result->bits_.exp = exp;
      result->bits_.sign = sign;
    }
  } else {
    *result = lo;
    result->bits_.exp = value_1.bits_.exp + value_2.bits_.exp;
    result->bits_.sign = sign;
  }
  return res;
}

int div(decimal_t value_1, decimal_t value_2, decimal_t *result) {
  if (is_empty(&value_2)) return 3;
  if (is_empty(&value_1)) return 0;
  int res = 0, exp = value_1.bits_.exp - value_2.bits_.exp;
  int sign = value_1.bits_.sign ^ value_2.bits_.sign;

  value_2.bits[3] = 0;
  *result = (decimal_t){0, 0, 0, 0};

  decimal_t div = {value_1.bits[0], value_1.bits[1], value_1.bits[2], 0};
  decimal_t tmp = {value_1.bits[0], value_1.bits[1], value_1.bits[2], 0};
  while (is_less_no_norm(div, value_2) && !scale96(&tmp)) {
    div = tmp;
    exp++;
  }

  if (!is_less_no_norm(div, value_2))
    res = integral(div, value_2, result, &div);
  if (!res && !is_empty(&div)) res = fract(div, value_2, result, &exp);
  for (; !res && is_empty(&div) && exp < 0 && exp; exp++) res = scale96(result);
  if (!res && exp > 28) {
    decimal_t hi = (decimal_t){0, 0, 0, 0};
    ;
    unscale(&hi, result, 28 - exp);
    exp = 28;
  }
  if (!res && exp < 0) res = sign ? 2 : 1;
  if (!res && !is_empty(result)) result->bits_.exp = exp;
  if (!res) result->bits_.sign = sign;
  return res;
}

int mod(decimal_t value_1, decimal_t value_2, decimal_t *result) {
  if (is_empty(&value_2)) return 3;
  if (is_empty(&value_1)) return 0;

  int sign = value_1.bits_.sign;
  int res = 0;

  decimal_t tmp = {0, 0, 0, 0};
  res = div(value_1, value_2, &tmp);
  truncate(tmp, &tmp);
  mul(value_2, tmp, &tmp);
  sub(value_1, tmp, result);
  result->bits_.sign = sign;
  return res;
}
