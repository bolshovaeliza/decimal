#include "decimal.h"
#include "decimal__extra.h"

int addition(decimal_t value_1, decimal_t value_2,
                 decimal_t* result) {
  *result = (decimal_t){0, 0, 0, result->bits[3]};
  int res = 0, mem = 0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 32; j++) {
      switch ((bit_check(value_1.bits[i], j) != 0) +
              (bit_check(value_2.bits[i], j) != 0) + (mem > 0)) {
        case 1:
          result->bits[i] = bit_add(result->bits[i], j);
          mem = 0;
          break;
        case 2:
          mem = 1 << (j + 1);
          if (mem < 0) mem = 1;
          break;
        case 3:
          result->bits[i] = bit_add(result->bits[i], j);
          mem = 1 << (j + 1);
          if (mem < 0) mem = 1;
          break;
        default:
          break;
      }
    }
  }
  if (mem) res = 1;
  return res;
}

int subtraction(decimal_t value_1, decimal_t value_2,
                    decimal_t* result) {
  int tmp1, tmp2, res = 0, mem = 0;
  *result = (decimal_t){0, 0, 0, result->bits[3]};
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 32; j++) {
      tmp1 = bit_check(value_1.bits[i], j);
      tmp2 = bit_check(value_2.bits[i], j);
      if (tmp1) {
        if (mem) {
          mem = 0;
          if (tmp2) {
            mem = 1 << j;
            if (j == 31) mem = 1;
            result->bits[i] = result->bits[i] | 1 << j;
          }
        } else {
          result->bits[i] = result->bits[i] | tmp1 - tmp2;
        }
      } else {
        if (mem) {
          if (tmp2) {
            mem = mem;
          } else {
            result->bits[i] = result->bits[i] | 1 << j;
          }
        } else {
          if (tmp2) {
            mem = 1 << j;
            if (j == 31) mem = 1;
            result->bits[i] = result->bits[i] | 1 << j;
          }
        }
      }
    }
  }
  if (mem) res = 1;
  return res;
}

int multi_by_10(decimal_t* val) {
  decimal_t temp2, temp1 = temp2 = *val;
  int res = 0;
  res += shift(&temp1, 1);
  res += shift(&temp2, 3);
  if (!res) {
    decimal_t temp3;
    nulling_decimal(&temp3);
    res += addition(temp1, temp2, &temp3);
    temp3.bits_.exp = val->bits_.exp;
    temp3.bits_.sign = val->bits_.sign;
    if (!res) equating_decimal(temp3, val);
  }
  return res;
}

int div_by_x(decimal_t* val, int x) {
  decimal_t src = *val, dst = {0, 0, 0, 0}, temp = {0, 0, 0, 0},
              x_dec = {x, 0, 0, 0};
  x_dec.bits_.exp = val->bits_.exp;
  int shift_num = num_len(*val, 2) - (num_len(x_dec, 2));
  if (shift_num < 0) shift_num = 0;
  shift(&x_dec, shift_num);
  src.bits_.sign = 0;
  while (1) {
    shift(&dst, 1);
    if (is_less_no_norm(src, x_dec)) {
      if (x_dec.bits[0] == x) break;
    } else {
      dst.bits[0] = bit_add(dst.bits[0], 0);
      subtraction(src, x_dec, &temp);
      equating_decimal(temp, &src);
      if (x_dec.bits[0] == x) break;
    }
    shift(&x_dec, -1);
  }
  nulling_decimal(&temp);
  equating_decimal(dst, &temp);
  multi_by_10(&temp);
  subtraction(*val, temp, &temp);
  dst.bits[3] = val->bits[3];
  equating_decimal(dst, val);
  return temp.bits_.byte_0;
}

int get_fraction_dec(decimal_t fract, decimal_t* res, int trunc) {
  int flag = 1, pow = 0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 32; j++) {
      shift(&fract, 1);
      if (flag && !trunc) {
        pow--;
        j--;
      }
      if (fract.bits_.exp < num_len(fract, 10)) {
        flag = 0;
        res->bits[i] = res->bits[i] | 1 << j;
        sub(fract, (decimal_t){1, 0, 0, 0}, &fract);
      }
    }
  }
  return pow;
}

int bank_round(decimal_t* val, round_struct* data) {
  int res = 0, last_num = 0, last_last_num = 0;
  decimal_t temp = {0, 0, 0, 0};
  if (val->bits_.exp > 0) {
    last_num = div_by_x(val, 10);
    val->bits_.exp--;
    if (last_num > 0) {
      decimal_t one = {1, 0, 0, 0};
      one.bits_.exp = val->bits_.exp;
      if (last_num != 5) {
        data->prev_5r = 0;
        if (last_num > 5) {
          addition(*val, one, &temp);
          res = last_num;
          data->prev = 1;
          temp.bits[3] = val->bits[3];
          equating_decimal(temp, val);
        } else {
          data->prev = 0;
        }
        equating_decimal(*val, &temp);
        last_last_num = div_by_x(&temp, 10);
      } else {
        equating_decimal(*val, &temp);
        last_last_num = div_by_x(&temp, 10);
        if (data->prev) data->prev_5r = 1;
        if ((!data->prev && last_last_num % 2 != 0) ||
            (!data->prev && data->befor_rounded)) {
          addition(*val, one, &temp);
          res = last_num;
          data->prev = 1;
        } else {
          equating_decimal(*val, &temp);
          data->prev = 0;
        }
        temp.bits[3] = val->bits[3];
        equating_decimal(temp, val);
      }
    }
  }
  if (last_num == 0) {
    equating_decimal(*val, &temp);
    last_last_num = div_by_x(&temp, 10);
  }
  data->lost_int = last_num;
  data->last_last_num = last_last_num;
  if (last_num) data->befor_rounded++;
  return res;
}

int collect_int(const char** p, int* count_num) {
  int res = 0;
  for (; (*count_num < 7) && (*(*p) != '.'); (*count_num)++, (*p)++) {
    res = res * 10 + (**p - 48);
  }
  return res;
}

int multi_by_10_x4(decimal_t* val) {
  decimal_t temp2, temp1 = temp2 = *val;
  int res = 0;
  res += shift_x4(&temp1, 1);
  res += shift_x4(&temp2, 3);
  if (!res) {
    decimal_t temp3;
    nulling_decimal(&temp3);
    res += addition_x4(temp1, temp2, &temp3);
    if (!res) equating_decimal(temp3, val);
  }
  return res;
}

int scale96(decimal_t* val) {
  multi_by_10_x4(val);
  return val->bits[3] != 0;
}

int addition_x4(decimal_t value_1, decimal_t value_2,
                    decimal_t* result) {
  int res = 0, mem = 0;
  nulling_decimal(result);
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 32; j++) {
      switch ((bit_check(value_1.bits[i], j) != 0) +
              (bit_check(value_2.bits[i], j) != 0) + (mem > 0)) {
        case 1:
          result->bits[i] = bit_add(result->bits[i], j);
          mem = 0;
          break;
        case 2:
          mem = 1 << (j + 1);
          if (mem < 0) mem = 1;
          break;
        case 3:
          result->bits[i] = bit_add(result->bits[i], j);
          mem = 1 << (j + 1);
          if (mem < 0) mem = 1;
          break;
        default:
          break;
      }
    }
  }
  if (mem) res = 1;
  return res;
}

int subtraction_x4(decimal_t value_1, decimal_t value_2,
                       decimal_t* result) {
  int tmp1, tmp2, res = 0, mem = 0;
  nulling_decimal(result);
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 32; j++) {
      tmp1 = bit_check(value_1.bits[i], j);
      tmp2 = bit_check(value_2.bits[i], j);

      if (tmp1) {
        if (mem) {
          mem = 0;
          if (tmp2) {
            mem = 1 << j;
            if (j == 31) mem = 1;
            result->bits[i] = result->bits[i] | 1 << j;
          }
        } else {
          result->bits[i] = result->bits[i] | tmp1 - tmp2;
        }

      } else {
        if (mem) {
          if (tmp2) {
            mem = mem;
          } else {
            result->bits[i] = result->bits[i] | 1 << j;
          }
        } else {
          if (tmp2) {
            mem = 1 << j;
            if (j == 31) mem = 1;
            result->bits[i] = result->bits[i] | 1 << j;
          }
        }
      }
    }
  }
  if (mem) res = 1;
  return res;
}

int fract(decimal_t value_1, decimal_t value_2, decimal_t* result,
          int* exp) {
  int res = 0, point_pos = 0, digit = 0, scale = 0, end = 0;
  decimal_t tmp, fract = tmp = (decimal_t){0, 0, 0, 0};
  for (; !is_empty(&value_1); point_pos++) {
    left_x4(&fract);
    left_x4(&value_1);
    if (mantiss_cmp(value_1, value_2)) {
      fract.bits[0] |= 1;
      subtraction_x4(value_1, value_2, &value_1);
    }
    if (is_empty(&value_1) || point_pos == 112) break;
  }
  for (int max = *exp; !end && max < 28 && !is_empty(&fract); max++) {
    tmp = *result;
    digit = push_integer_x4(&fract, point_pos);
    if (end = scale96(&tmp)) break;
    addition(tmp, (decimal_t){digit, 0, 0, 0}, &tmp);
    *result = tmp;
    (*exp)++;
    if (scale96(&tmp)) break;
  }
  if (!is_empty(&fract)) {
    if (!end) digit = push_integer_x4(&fract, point_pos);
    if (digit >= 5) {
      for (; scale && *exp < 28; scale--) {
        multi_by_10_x4(result);
        (*exp)++;
      }
      addition(*result, (decimal_t){1, 0, 0, 0}, result);
    }
  }
  decimal_t f = {0, 0, 0, 0}, i = *result;
  while ((*exp) > 0 && !integral(i, (decimal_t){10, 0, 0, 0}, &i, &f) &&
         !is_empty(&i) && is_empty(&f)) {
    (*exp)--;
    div_by_x(result, 10);
    i = *result;
  }
  return res;
}

int integral(decimal_t value_1, decimal_t value_2, decimal_t* result,
             decimal_t* fract) {
  int res = 0;
  int shift = 0;
  *result = (decimal_t){0, 0, 0, 0};
  *fract = (decimal_t){value_1.bits[0], value_1.bits[1], value_1.bits[2], 0};
  for (; shift < 96; shift++) {
    decimal_t tmp = {fract->bits[0], fract->bits[1], fract->bits[2], 0};
    right(&tmp);
    if (!is_less_no_norm(tmp, value_2))
      *fract = (decimal_t){tmp.bits[0], tmp.bits[1], tmp.bits[2], 0};
    else
      break;
  }
  for (; !is_less_no_norm(*fract, value_2) || shift >= 0; shift--) {
    left(result);
    if (!is_less_no_norm(*fract, value_2)) {
      result->bits[0] |= 1;
      subtraction(*fract, value_2, fract);
    }
    if (shift > 0) {
      left(fract);
      fract->bits[0] |= bit_get_x4(value_1, shift - 1);
    } else
      break;
  }
  return res;
}

int add_with_round(decimal_t a, decimal_t b, round_struct data,
                   decimal_t* result) {
  round_struct br_a = {0}, br_b = {0};
  decimal_t dec_one = {1, 0, 0, 0}, temp = {0, 0, 0, 0};
  while (addition(a, b, result) && result->bits_.exp != 255) {
    bank_round(&a, &br_a);
    bank_round(&b, &br_b);
    result->bits_.exp--;
  }
  temp = *result;
  int res = div_by_x(&temp, 10);
  if (data.lost_int == 5 && data.befor_rounded == 1) {
    if (res % 2 != 0 && !data.prev_5r) {
      if ((data.norm_last_int % 2 != 0) == (data.last_last_num % 2 != 0)) {
        subtraction(*result, dec_one, result);
      } else {
        addition(*result, dec_one, result);
      }
    }
  }
  if (br_a.prev || br_b.prev) {
    if (br_a.lost_int + br_b.lost_int <= 15) {
      if ((br_a.prev && br_b.prev ||
           (br_a.last_last_num % 2 != 0 && br_b.last_last_num % 2 != 0 &&
            br_a.prev + br_b.prev == 1 &&
            (br_a.lost_int == 5 || br_b.lost_int == 5))) &&
          !(br_a.lost_int + br_b.lost_int == 15 &&
            (br_a.last_last_num + br_b.last_last_num) % 2 == 0)) {
        subtraction(*result, dec_one, result);
      }
    }
  }
  temp = *result;
  if (!br_a.prev && !br_b.prev && br_a.lost_int + br_b.lost_int >= 5) {
    if (br_a.lost_int + br_b.lost_int == 5 &&
        (br_a.last_last_num + br_b.last_last_num) % 2 == 0) {
    } else {
      addition(*result, dec_one, result);
    }
  }
  return (result->bits_.exp == 255);
}

void subtraction_correction(decimal_t* result, round_struct* round_data) {
  decimal_t tmp = {0, 0, 0, 0};
  if (num_len(*result, 10) <= 28 && round_data->d_scaled) {
    tmp = *result;
    if (!multi_by_10(&tmp)) {
      if (round_data->prev) {
        *result = tmp;
        if (round_data->lost_int)
          addition(*result,
                       (decimal_t){10 - round_data->lost_int, 0, 0, 0},
                       result);
      } else {
        subtraction(tmp, (decimal_t){round_data->lost_int, 0, 0, 0},
                        result);
      }
      result->bits_.exp++;
    } else {
      if (round_data->last_last_num % 2 != 0 &&
          round_data->norm_last_int % 2 != 0 && round_data->lost_int == 5 &&
          round_data->befor_rounded == 1)
        addition(*result, (decimal_t){1, 0, 0, 0}, result);
    }
  }
  if (round_data->lost_int == 5 && num_len(*result, 10) == 29 &&
      round_data->befor_rounded == 1 && round_data->norm_last_int % 2 != 0) {
    if (round_data->last_last_num % 2 != 0) {
      addition(*result, (decimal_t){1, 0, 0, 0}, result);
    } else {
      subtraction(*result, (decimal_t){1, 0, 0, 0}, result);
    }
  }
}
int mul_sub(decimal_t value_1, decimal_t value_2, decimal_t* result) {
  *result = (decimal_t){0, 0, 0, 0};
  int res = 0;
  while (!res && !is_empty(&value_1) && !is_empty(&value_2)) {
    if (value_2.bits[0] & 1) {
      res = addition_x4(value_1, *result, result);
    }
    left(&value_1);
    right(&value_2);
  }
  return res;
}

int unscale(decimal_t* hi, decimal_t* lo, int down) {
  int res = 0;
  decimal_t integ, fract = integ = (decimal_t){0, 0, 0, 0};
  decimal_t tmp, tmp_fract;

  while (down < 0 /*&& !is_empty(lo)*/ || !is_empty(hi)) {
    integral(*hi, (decimal_t){10, 0, 0, 0}, hi, &fract);
    integral((decimal_t){lo->bits[2], fract.bits[0], 0, 0},
             (decimal_t){10, 0, 0, 0}, &integ, &fract);
    integral((decimal_t){lo->bits[0], lo->bits[1], fract.bits[0], 0},
             (decimal_t){10, 0, 0, 0}, lo, &tmp_fract);
    addition_x4(*lo, (decimal_t){0, 0, integ.bits[0], 0}, lo);
    addition_x4(*hi, (decimal_t){lo->bits[3], 0, 0, 0}, hi);
    lo->bits[3] = 0;
    down++;
  }
  if (tmp_fract.bits[0] >= 5 && !is_empty(lo))
    addition_x4(*lo, (decimal_t){1, 0, 0, 0}, lo);

  return down;
}

void mul_hi_lo(decimal_t value_1, decimal_t value_2, decimal_t* hi,
               decimal_t* lo) {
  decimal_t tmp, add;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      mul_sub((decimal_t){value_1.bits[i], 0, 0, 0},
              (decimal_t){value_2.bits[j], 0, 0, 0}, &tmp);
      add = (decimal_t){0, 0, 0, 0};
      if (i + j < 3) {
        add.bits[i + j] = tmp.bits[0];
        addition_x4(*lo, add, lo);
        addition_x4(*hi, (decimal_t){lo->bits[3], 0, 0, 0}, hi);
        lo->bits[3] = 0;
      } else {
        add.bits[(i + j) % 3] = tmp.bits[0];
        addition_x4(*hi, add, hi);
      }
      add = (decimal_t){0, 0, 0, 0};
      if (i + j + 1 < 3) {
        add.bits[i + j + 1] = tmp.bits[1];
        addition_x4(*lo, add, lo);
        addition_x4(*hi, (decimal_t){lo->bits[3], 0, 0, 0}, hi);
        lo->bits[3] = 0;
      } else {
        add.bits[(i + j + 1) % 3] = tmp.bits[1];
        addition_x4(*hi, add, hi);
      }
    }
  }
}
