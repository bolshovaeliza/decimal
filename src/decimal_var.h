#ifndef SRC_DECIMAL_VAR_H_
#define SRC_DECIMAL_VAR_H_

typedef unsigned int uint_t;

typedef struct {
  int byte_0 : 32;
  int byte_1 : 32;
  int byte_2 : 32;
  unsigned : 16;
  unsigned exp : 8;
  unsigned : 7;
  unsigned sign : 1;
} decimal_uint;

typedef union {
  int bits[4];
  decimal_uint bits_;
  unsigned char octets[16];
} decimal_t;

typedef struct {
  unsigned fract : 23;
  unsigned exp : 8;
  unsigned sign : 1;
} ieee;

typedef union {
  int i;
  float fl;
  ieee s_fl;
} ieee_754;

typedef struct {
  int prev;
  int befor_rounded;
  int lost_int;
  int last_last_num;
  int switched;
  int b_less;
  int norm_last_int;
  int prev_5r;
  int d_scaled;
  int m_scaled;
} round_struct;

#endif  // SRC_DECIMAL_VAR_H_
