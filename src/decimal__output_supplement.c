#include <stdint.h>
#include <stdio.h>

#include "decimal__extra.h"

int print_one_bit(unsigned int byte, int before_dot) {
  for (unsigned int i = 32; i > 0; i--) {
    if (i % 8 == 0) printf(" ");
    if (byte & ((unsigned)1 << (i - 1)))
      printf("1");
    else
      printf("0");
    before_dot--;
    if (!before_dot) printf(".");
  }
  printf(" |");
  return before_dot;
}

// void output(decimal_t val) {
//   union to_print temp;
//   for (int i = 0; i < 3; i++) temp.bits[i] = val.bits[i];
//   // if (val.bits_.exp == 1) temp.print = -temp.print;
//   if (val.bits_.sign) printf("-");
//   // printf("%lld\n",temp.print);

//   printf("%lld\n", (unsigned long long)(temp.print & 0xFFFFFFFFFFFFFFFF));
// }

void output_bit_rep(decimal_t a) {
  if (a.bits_.sign) printf("-");
  int before_dot = 1000 - a.bits_.exp;
  before_dot = print_one_bit(a.bits[2], before_dot);
  before_dot = print_one_bit(a.bits[1], before_dot);
  before_dot = print_one_bit(a.bits[0], before_dot);
  printf("  exp = %d", a.bits_.exp);
  printf("\n");
}

void decimal_to_hex(decimal_t val) {
  char *tmp = (char *)&val;
  for (uint_t i = sizeof(decimal_t); i; i--) {
    printf("%02x ", tmp[i - 1] & 0xFF);
  }
  printf("\n");
}

void sharp_dec(decimal_t res) {
  printf("var res = new decimal(%d, %d, %d, %s, %d);\n", res.bits[0],
         res.bits[1], res.bits[2],
         (res.bits_.sign) ? "true" : "false", res.bits_.exp);
}
