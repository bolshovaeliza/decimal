#ifndef SRC_DECIMAL_H_
#define SRC_DECIMAL_H_

#include "decimal_var.h"

//=========================
// Арифметические операторы
//=========================
/*
    Сумма двух decimal
    @param value_1 первое число
    @param value_2 второе число
    @param *result сумма чисел

    @return 0 - OK
    @return 1 - число слишком велико или равно бесконечности
    @return 2 - число слишком мало или равно отрицательной бесконечности
    @return 3 - деление на 0
*/
int add(decimal_t value_1, decimal_t value_2, decimal_t *result);

/*
    Вычитание двух decimal, из большего меньшее
    @param value_1 первое число
    @param value_2 второе число
    @param *result разница чисел

    @return 0 - OK
    @return 1 - число слишком велико или равно бесконечности
    @return 2 - число слишком мало или равно отрицательной бесконечности
    @return 3 - деление на 0
*/
int sub(decimal_t value_1, decimal_t value_2, decimal_t *result);

/*
    Умножение
    0 - OK
    1 - число слишком велико или равно бесконечности
    2 - число слишком мало или равно отрицательной бесконечности
*/
int mul(decimal_t value_1, decimal_t value_2, decimal_t *result);

/*
    Деление
    0 - OK
    1 - число слишком велико или равно бесконечности
    2 - число слишком мало или равно отрицательной бесконечности
    3 - деление на 0
*/
int div(decimal_t value_1, decimal_t value_2, decimal_t *result);

/*
    Остаток от деления Mod
*/
int mod(decimal_t value_1, decimal_t value_2, decimal_t *result);

//====================
// Операторы сравнения
//====================

/*
    Меньше
*/
int is_less(decimal_t a, decimal_t b);
/*
    Меньше или равно
*/
int is_less_or_equal(decimal_t a, decimal_t b);
/*
    Больше
*/
int is_greater(decimal_t a, decimal_t b);
/*
    Больше или равно
*/
int is_greater_or_equal(decimal_t a, decimal_t b);
/*
    Равно
*/
int is_equal(decimal_t a, decimal_t b);
/*
    Не равно
*/
int is_not_equal(decimal_t a, decimal_t b);

//================
// Преобразователи
//================

/*
    Из int
*/
int from_int_to_decimal(int src, decimal_t *dst);

/*
    Из float
*/
int from_float_to_decimal(float src, decimal_t *dst);
/*
    В int
*/
int from_decimal_to_int(decimal_t src, int *dst);

/*
    В float
*/
int from_decimal_to_float(decimal_t src, float *dst);

//===============
// Другие функции
//===============

/*
    Округляет указанное decimal_t число до ближайшего целого числа в сторону
    отрицательной бесконечности.
*/
int decimal_floor(decimal_t value, decimal_t *result);

/*
    Округляет decimal_t до ближайшего целого числа.
*/
int decimal_round(decimal_t value, decimal_t *result);

/*
    Возвращает целые цифры указанного decimal_t числа;
    любые дробные цифры отбрасываются, включая конечные нули.
*/
int truncate(decimal_t value, decimal_t *result);

/*
    Возвращает результат умножения указанного decimal_t на - 1.
*/
int negate(decimal_t value, decimal_t *result);

#endif  //  SRC_DECIMAL_H_