#include "decimal__extra.h"

int bit_check(uint_t num, uint_t bit_num) {
  return num & (1 << bit_num);
}

int bit_remove(uint_t num, uint_t bit_num) {
  return num ^ (1 << bit_num);
}

int bit_add(uint_t num, uint_t bit_num) { return num | (1 << bit_num); }

int left(decimal_t* val) {
  int res = bit_check(val->bits[2], 31);
  val->bits[2] <<= 1;
  val->bits[2] = val->bits[2] | (val->bits[1] & 0x80000000) >> 31;
  val->bits[1] <<= 1;
  val->bits[1] = val->bits[1] | (val->bits[0] & 0x80000000) >> 31;
  val->bits[0] <<= 1;
  return res;
}

int left_x4(decimal_t* val) {
  int res = bit_check(val->bits[3], 31);
  for (int i = 3; i; i--) {
    val->bits[i] <<= 1;
    val->bits[i] = val->bits[i] | (val->bits[i - 1] & 0x80000000) >> 31;
  }
  val->bits[0] <<= 1;
  return res;
}

int right(decimal_t* val) {
  int res = bit_check(val->bits[0], 0);
  val->bits[0] = (val->bits[0] >> 1) & 0x7FFFFFFF;
  val->bits[0] = val->bits[0] | (val->bits[1] & 0x1) << 31;
  val->bits[1] = (val->bits[1] >> 1) & 0x7FFFFFFF;
  val->bits[1] = val->bits[1] | (val->bits[2] & 0x1) << 31;
  val->bits[2] = (val->bits[2] >> 1) & 0x7FFFFFFF;
  return res;
}

int right_x4(decimal_t* val) {
  int res = bit_check(val->bits[0], 0);
  for (int i = 0; i < 3; i++) {
    val->bits[i] = (val->bits[i] >> 1) & 0x7FFFFFFF;
    val->bits[i] = val->bits[i] | (val->bits[i + 1] & 0x1) << 31;
  }
  val->bits[3] = (val->bits[3] >> 1) & 0x7FFFFFFF;

  return res;
}

int shift(decimal_t* val, int shift_num) {
  decimal_t temp;
  equating_decimal(*val, &temp);
  if (shift_num < 0) {
    while ((shift_num < 0) & (!right(&temp))) {
      right(val);
      shift_num++;
    }
  } else {
    while ((shift_num > 0) & (!left(&temp))) {
      left(val);
      shift_num--;
    }
  }
  return shift_num;
}

int shift_x4(decimal_t* val, int shift_num) {
  decimal_t temp;
  equating_decimal(*val, &temp);
  if (shift_num < 0) {
    while ((shift_num < 0) & (!right_x4(&temp))) {
      right_x4(val);
      shift_num++;
    }
  } else {
    while ((shift_num > 0) & (!left_x4(&temp))) {
      left_x4(val);
      shift_num--;
    }
  }
  return shift_num;
}

int is_less_no_norm(decimal_t a, decimal_t b) {
  int res = 0, reps = 0, num = 2, pos = 31;
  while ((num > -1)) {
    int a_check = bit_check(a.bits[num], pos);
    if (a_check != 0 && a_check != 1) a_check = 1;
    int b_check = bit_check(b.bits[num], pos);
    if (b_check != 0 && b_check != 1) b_check = 1;
    if (a_check < b_check) {
      res = 1;
      break;
    } else if (a_check > b_check) {
      break;
    } else {
      pos--;
      if (pos == -1) {
        pos = 31;
        num--;
      }
    }
    reps++;
  }
  return res;
}

int bit_get_x4(decimal_t val, unsigned char pos) {
  if (pos > 127) return 0;
  unsigned res = 1 << pos % 32;
  res &= val.bits[pos / 32];
  return res > 0;
}

void bit_and_x4(decimal_t* a, decimal_t b) {
  for (int i = 0; i < 4; i++) a->bits[i] &= b.bits[i];
}
