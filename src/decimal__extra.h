#ifndef SRC_DECIMAL_EXTRA_H_
#define SRC_DECIMAL_EXTRA_H_

#include "decimal_var.h"

//=========================
// Побитовые операции
//=========================

/*
Возвращает 2^bit_num, если бит отмечен, 0 в ином случае
*/
int bit_check(uint_t num, uint_t bit_num);

/*
Возвращает число num с удаленным битом bit_num
*/
int bit_remove(uint_t num, uint_t bit_num);

/*
Возвращает число num с добавленным битом bit_num
*/
int bit_add(uint_t num, uint_t bit_num);

/*
Сдвигает биты мантиссы влево (<<)
Возвращает 1, если успешно, и 0, если сдвиг невозможен без потери бита.
*/
int left(decimal_t* val);

/*
Сдвигает биты мантиссы вправо (>>).
Возвращает 1, если успешно, и 0, если сдвиг невозможен без потери бита.
*/
int right(decimal_t* val);

/*
Сдвигает биты мантиссы на указанное значение.
Если shift_num > 0, сдвиг влево на указанное значение, иначе вправо.
Возвращает 0 при успехе. Иначе возвращает число, на которое не
удалось сдвинуть без потери.
*/
int shift(decimal_t* val, int shift_num);

/*
Сравнение a и b. Если a<b, результат 1, иначе 0.
Функция не использует bits[3], т.е. нет проверки на знаки и экспоненту.
*/
int is_less_no_norm(decimal_t a, decimal_t b);

//=========================
// Операции со структурой (aka struct wrangling)
//=========================
/*
Зануление decimal
*/
void nulling_decimal(decimal_t* decimal);

/*
Приравнивание всех полей decimal
*/
void equating_decimal(decimal_t src, decimal_t* dst);
/*
Сравнение знаков двух decimal, 1 если одинаковый, 0 если разный
*/
int compare_sign(decimal_t a, decimal_t b);

/*
Сравнение мантисс двух decimal, 1 если одинаковая, 0 если разная
*/
int compare_val(decimal_t a, decimal_t b);

/*
Проверяет мантиссу на 0
*/
int is_empty(const decimal_t* val);

/*
Считает длину целого числа в битах
*/
int count_bits(unsigned int num, unsigned int base);

/*
Считает длину decimal_t в битах
*/
int num_len(decimal_t a, unsigned int base);

/*
Нормализует 2 decimal_t (приводит к одинаковой exp).
*/
round_struct normalization(decimal_t* a, decimal_t* b);

//=========================
// Arithmetics
//=========================
/*
    сумма двух decimal
    @param value_1 первое число
    @param value_2 второе число
    @param *result сумма чисел

    @return 0 - OK
    @return 1 - число слишком велико или равно бесконечности
    @return 2 - число слишком мало или равно отрицательной бесконечности
    @return 3 - деление на 0
*/
int addition(decimal_t value_1, decimal_t value_2, decimal_t* result);

/*
    вычитание двух decimal, из большего меньшее
    @param value_1 первое число
    @param value_2 второе число
    @param *result разница чисел

    @return 0 - OK
    @return 1 - число слишком велико или равно бесконечности
    @return 2 - число слишком мало или равно отрицательной бесконечности
    @return 3 - деление на 0
*/
int subtraction(decimal_t value_1, decimal_t value_2,
                    decimal_t* result);

/*
Умножение на 10
*/
int multi_by_10(decimal_t* val);

/*
Деление на целочисленное x
*/
int div_by_x(decimal_t* val, int x);
/*
    дробная часть для записи во флоат
*/
int get_fraction_dec(decimal_t fract, decimal_t* res, int trunc);
/*
banker's rounding
*/
int bank_round(decimal_t* val, round_struct* data);
/*
собирает инт из char
*/
int collect_int(const char** p, int* count_num);

int add_with_round(decimal_t a, decimal_t b, round_struct data,
                   decimal_t* result);

void subtraction_correction(decimal_t* result, round_struct* round_data);
//=========================
// Для вывода
//=========================

/*
Печать decimal_t в битовом представлении
*/
void output_bit_rep(decimal_t a);

/*
Output decimal_t in a decimal_t form
*/
void output(decimal_t val);

/*
???
*/
void decimal_to_hex(decimal_t val);

/*
Печать uint в битовом представлении
*/
int print_one_bit(unsigned int byte, int before_dot);
void sharp_dec(decimal_t res);

/*
Сравнение мантисс
> 0, val1 > val2
< 0, val1 < val2
= 0, val1 = val2
*/
int mantiss_cmp(decimal_t val1, decimal_t val2);

/*
Значение по адресу в диапазоне 0-127,
или 0 за пределами диапазона
*/
int bit_get_x4(decimal_t val, unsigned char pos);

void bit_and_x4(decimal_t* a, decimal_t b);

// void bit_or(decimal_t* a, decimal_t b);

int left_x4(decimal_t* val);

int right_x4(decimal_t* val);

int shift_x4(decimal_t* val, int shift_num);

int multi_by_10_x4(decimal_t* val);

int addition_x4(decimal_t value_1, decimal_t value_2,
                    decimal_t* result);

int subtraction_x4(decimal_t value_1, decimal_t value_2,
                       decimal_t* result);

/*
получает дробную часть и позицию двоичной запятой (позиция старшего бита дробной
части) возвращает целое число, при этом в val меняется значение = результат
умножения дробной части на 10
*/
int push_integer_x4(decimal_t* val, unsigned char point_pos);

int is_empty_x4(const decimal_t* val);
/*
умножение на 10, если результат выходит за 96 бит возвращает 1, иначе 0
*/
int scale96(decimal_t* val);
/*
вычисление целой части частного от деления, value_1 > value_2
в fract остаток от деления
*/
int integral(decimal_t value_1, decimal_t value_2, decimal_t* result,
             decimal_t* fract);
/*
деление с двоичной запятой, value_1 < value_2
*/
int fract(decimal_t value_1, decimal_t value_2, decimal_t* result,
          int* exp);

/*
 */
int mul_sub(decimal_t value_1, decimal_t value_2, decimal_t* result);

int unscale(decimal_t* hi, decimal_t* lo, int unscale);

void mul_hi_lo(decimal_t value_1, decimal_t value_2, decimal_t* hi,
               decimal_t* lo);

#endif  // SRC_DECIMAL_EXTRA_H_