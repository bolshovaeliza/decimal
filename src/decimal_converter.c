#include <math.h>
#include <stdio.h>
#include <string.h>

#include "decimal.h"
#include "decimal__extra.h"

uint_t invert_bits(uint_t num) {
  uint_t res = 0;
  for (uint_t i = 0; i < 31; i++)
    if (!bit_check(num - 1, i)) res = bit_add(res, i);
  return res;
}

int from_int_to_decimal(int src, decimal_t *dst) {
  nulling_decimal(dst);
  if (src < 0) {
    dst->bits_.sign = 1;
    dst->bits[0] = invert_bits(src);
  } else {
    dst->bits[0] = (uint_t)src;
  }
  return 0;
}

int from_decimal_to_int(decimal_t src, int *dst) {
  int res = 0;
  decimal_t temp;
  nulling_decimal(&temp);
  truncate(src, &temp);
  if (temp.bits[1] != 0 || temp.bits[2] != 0)
    res = 1;
  else if (bit_check(temp.bits[0], 31) != 0)
    res = 1;
  if (temp.bits_.sign && !res) {
    if (temp.bits[0]) {
      *dst = invert_bits(temp.bits[0]);
      *dst = bit_add(*dst, 31);
    } else {
      *dst = 0;
    }
  } else {
    *dst = temp.bits[0];
  }
  return res;
}

int from_float_to_decimal(float src, decimal_t *dst) {
  nulling_decimal(dst);
  if (src < 0) {
    dst->bits_.sign = 1;
    src = -src;
  }
  if (((src < 0.0000000000000000000000000001) && src) ||
      (src >= 7.9228161E+28) || (src == INFINITY)) {
    return 1;
  }
  char temp[100];
  int len_fract, trunc, fract, count_num = 0;
  decimal_t fract_d = {0, 0, 0, 0}, tmp = {0, 0, 0, 0};
  sprintf(temp, "%10.8f", src);
  const char *p_temp = temp;
  trunc = collect_int(&p_temp, &count_num);
  if (!trunc) count_num = 0;
  while (*p_temp != '.' && count_num >= 7) {
    if (count_num == 7 && *p_temp - 48 > 5) trunc++;
    count_num++;
    p_temp++;
  }
  p_temp++;
  fract = collect_int(&p_temp, &count_num);
  len_fract = log10(fract) + 1;
  dst->bits_.byte_0 = trunc;
  if (len_fract > 0) {
    dst->bits_.exp = len_fract;
    if (*p_temp - 48 > 5) fract++;
    fract_d.bits_.byte_0 = fract;
    while (len_fract) {
      multi_by_10(dst);
      len_fract--;
    }
    addition(*dst, fract_d, dst);
  }
  equating_decimal(*dst, &tmp);
  while (tmp.bits_.exp && !div_by_x(&tmp, 10)) {
    tmp.bits_.exp--;
    equating_decimal(tmp, dst);
  }
  while (count_num - 7 > 0) {
    multi_by_10(dst);
    count_num--;
  }
  return 0;
}

int from_decimal_to_float(decimal_t src, float *dst) {
  int exp = 0, pow = 0;
  ieee_754 res_float = {0};
  decimal_t trunc_res = {0, 0, 0, 0}, fraction_res = {0, 0, 0, 0},
              tmp = {0, 0, 0, 0}, part2 = {0, 0, 0, 0};
  truncate(src, &trunc_res);
  equating_decimal(trunc_res, &tmp);
  for (int i = src.bits_.exp; i > 0; i--) multi_by_10(&tmp);
  subtraction(src, tmp, &fraction_res);
  fraction_res.bits_.exp = src.bits_.exp;
  int trunc = (trunc_res.bits[0] || trunc_res.bits[1] || trunc_res.bits[2]);
  int fract =
      (fraction_res.bits[0] || fraction_res.bits[1] || fraction_res.bits[2]);
  if (fract) {
    pow = get_fraction_dec(fraction_res, &part2, trunc);
  }
  if (trunc) {
    int s =
        ((trunc_res.bits[2] == -1) ||
         (!trunc_res.bits[2] && trunc_res.bits[1] == -1) ||
         (!trunc_res.bits[2] && trunc_res.bits[1] && trunc_res.bits[0] == -1));
    pow = s + (num_len(trunc_res, 2) - 1);
  }
  if (src.bits[0] || src.bits[1] || src.bits[2]) {
    for (int count = 96; (count > 0) && !shift(&trunc_res, 1); count--) {
      trunc_res.bits[0] =
          trunc_res.bits[0] | (bit_check(part2.bits[0], 0) != 0);
      right(&part2);
    }
    exp = 127 + pow;
  }
  res_float.s_fl.sign = src.bits_.sign;
  res_float.s_fl.exp = exp;
  if (pow >= 0) left(&trunc_res);
  for (int i = 22; i > -1; i--) {
    res_float.s_fl.fract =
        res_float.s_fl.fract | (bit_check(trunc_res.bits[2], 31) != 0) << i;
    left(&trunc_res);
  }
  if ((bit_check(trunc_res.bits[2], 31) != 0)) {
    res_float.s_fl.fract++;
  }
  *dst = res_float.fl;
  return 0;
}
