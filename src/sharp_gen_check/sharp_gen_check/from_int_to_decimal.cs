namespace sharp_gen_check
{
    public class FromIntToDecimal: FuncConvert
    {
        public FromIntToDecimal(int varInt) : base(varA:0)
        {
            TestName = "from_int_to_decimal";
            VarInt = varInt;
        }

        protected override void WriteVar()
        {
            base.WriteVar();
            File.WriteLine("\t int {0} = {1};", NameVarInt, VarInt);
        }

        protected override void WriteFunk()
        {
            File.WriteLine("\t int {2} = from_int_to_decimal({0}, &{1});", NameVarInt, NameRes, NameResReturn);
            BitsRes = decimal.GetBits(Convert.ToDecimal(VarInt));
        }
    }
}