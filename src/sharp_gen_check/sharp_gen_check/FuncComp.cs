namespace sharp_gen_check;

public abstract class FuncComp: TestCase
{
    protected FuncComp(decimal varA, decimal varB) : base(varA, varB)
    {
    }

    protected override void WriteVar()
    {            
        FileWriteVar(VarA, NameVarA);
        FileWriteVar(VarB, NameVarB);
    }

    protected override void WriteAssert()
    {
        File.WriteLine("\t ck_assert_int_eq({0}, {1});", NameResReturn, ResReturn);
    }
}