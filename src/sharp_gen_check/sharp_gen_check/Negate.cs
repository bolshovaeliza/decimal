namespace sharp_gen_check
{
    public class Negate : FuncOther
    {
        public Negate(decimal varA) : base(varA)
        {
            TestName = "negate";
        }

        protected override void WriteFunk()
        {
            File.WriteLine("\t int {2} = negate({0}, &{1});", NameVarA, NameRes, NameResReturn);
            BitsRes = decimal.GetBits(VarA * -1m);
        }
    }
}