namespace sharp_gen_check;

public class Floor: FuncOther
{
    public Floor(decimal varA) : base(varA)
    {
        TestName = "floor";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = decimal_floor({0}, &{1});", NameVarA, NameRes, NameResReturn);
        BitsRes = decimal.GetBits(decimal.Floor(VarA) );
    }
}