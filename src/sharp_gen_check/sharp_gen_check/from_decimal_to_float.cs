namespace sharp_gen_check;

public class FromDecimalToFloat : FuncConvert
{
    public FromDecimalToFloat(decimal varA) : base(varA)
    {
        TestName = "from_decimal_to_float";
        VarA = varA;
    }

    // protected override void InitTest()
    // {
    //     File.WriteLine("#test {0}_{1}", TestName, TestNum);
    // }

    protected override void WriteVar()
    {
        FileWriteVar(VarA, NameVarA);
        File.WriteLine("\t float {0};", NameVarFloat);
    }
    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = from_decimal_to_float({0}, &{1});", NameVarA, NameResFloat, NameResReturn);
        ResFloat = Convert.ToSingle(VarA);
    }

    protected override void WriteAssert()
    {
        File.WriteLine("\t ck_assert_float_eq({0}, {1});", NameResFloat, Convert.ToSingle(VarA).ToString(System.Globalization.CultureInfo.InvariantCulture));
        File.WriteLine("\t ck_assert_int_eq({0}, {1});", NameResReturn, ResReturn);
    }
}