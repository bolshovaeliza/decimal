namespace sharp_gen_check;

public class Sub: FuncArithmetic
{
    public Sub(decimal varA, decimal varB) : base(varA, varB)
    {
        TestName = "sub";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {3} = sub({0}, {1}, &{2});", NameVarA, NameVarB, NameRes, NameResReturn);
        try
        {
            BitsRes = decimal.GetBits(VarA - VarB);
        }
        catch (OverflowException)
        {
            if (decimal.Sign(VarA) > 0)
            {
               
                    ResReturn = 1;
                    ResReturn_custom = 1;
                }
                else
                {
                    ResReturn = 2;
                    ResReturn_custom = 2;
                }
            }
        
        
    }
}