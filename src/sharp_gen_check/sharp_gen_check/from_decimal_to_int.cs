using System.Security;

namespace sharp_gen_check;

public class FromDecimalToInt: FuncConvert
{
    public FromDecimalToInt(decimal varA) : base(varA)
    {
        TestName = "from_decimal_to_int";
    }

    protected override void WriteVar()
    {
        FileWriteVar(VarA, NameVarA);
        File.WriteLine("\t int {0};", NameVarInt);
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = from_decimal_to_int({0}, &{1});", NameVarA, NameResInt, NameResReturn);
        try
        {
            ResInt = Convert.ToInt32(decimal.Truncate(VarA));
        }
        catch (OverflowException)
        {
            ResReturn = 1;
            ResReturn_custom = 1;
        }
    }

    protected override void WriteAssert()
    {
        if (ResReturn == 0)
        {
            File.WriteLine("\t ck_assert_int_eq({0}, {1});", NameResInt, Convert.ToInt32(decimal.Truncate(VarA)).ToString());

        }
        File.WriteLine("\t ck_assert_int_eq({0}, {1});", NameResReturn, ResReturn); 
    }
}