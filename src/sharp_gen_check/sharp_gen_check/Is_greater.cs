namespace sharp_gen_check;

public class IsGreater: FuncComp
{
    public IsGreater(decimal varA, decimal varB) : base(varA, varB)
    {
        TestName = "is_greater";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = is_greater({0}, {1});", NameVarA, NameVarB, NameResReturn);
        ResReturn = Convert.ToInt32(VarA > VarB);
        ResReturn_custom = ResReturn;
    }
}