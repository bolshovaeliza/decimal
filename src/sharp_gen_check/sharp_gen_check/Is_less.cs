namespace sharp_gen_check;

public class IsLess: FuncComp
{
    public IsLess(decimal varA, decimal varB) : base(varA, varB)
    {
        TestName = "is_less";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = is_less({0}, {1});", NameVarA, NameVarB, NameResReturn);
        ResReturn = Convert.ToInt32(VarA < VarB);
        ResReturn_custom = ResReturn;
    }
}