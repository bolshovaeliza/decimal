namespace sharp_gen_check;

public class IsLessOrEqual: FuncComp
{
    public IsLessOrEqual(decimal varA, decimal varB) : base(varA, varB)
    {
        TestName = "is_less_or_equal";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = is_less_or_equal({0}, {1});", NameVarA, NameVarB, NameResReturn);
        ResReturn = Convert.ToInt32(VarA <= VarB);
        ResReturn_custom = ResReturn;
    }
}