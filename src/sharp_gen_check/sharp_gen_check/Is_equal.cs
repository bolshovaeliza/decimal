namespace sharp_gen_check;

public class IsEqual: FuncComp
{
    public IsEqual(decimal varA, decimal varB) : base(varA, varB)
    {
        TestName = "is_equal";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = is_equal({0}, {1});", NameVarA, NameVarB, NameResReturn);
        ResReturn =  Convert.ToInt32(Decimal.Equals( VarA, VarB ));
        ResReturn = ResReturn;
    }
}