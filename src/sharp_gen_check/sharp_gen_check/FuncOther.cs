namespace sharp_gen_check
{
    public abstract class FuncOther: TestCase
    {
        protected FuncOther(decimal varA) : base(varA, varB: 0m)
        {
        }

        protected override void InitTest()
        {
            base.InitTest();
            WriteInitRes();
        }

        protected override void WriteVar()
        {
            FileWriteVar(VarA, NameVarA);
        }
        

        protected override void WriteAssert()
        {
            AssertEqBits();
        }
    }
}