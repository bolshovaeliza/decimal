namespace sharp_gen_check;

public class Truncate: FuncOther
{
    public Truncate(decimal varA) : base(varA)
    {
        TestName = "truncate";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = truncate({0}, &{1});", NameVarA, NameRes, NameResReturn);
        BitsRes = decimal.GetBits(decimal.Truncate(VarA) );
    }
}