namespace sharp_gen_check
{
    public abstract class FuncArithmetic : TestCase
    {
    
        protected FuncArithmetic(decimal varA, decimal varB) : base(varA, varB)
        {
        }

        protected override void InitTest()
        {
            base.InitTest();
            WriteInitRes();
        }

        protected override void WriteVar()
        {
            FileWriteVar(VarA, NameVarA);
            FileWriteVar(VarB, NameVarB);
        }

        protected override void WriteAssert()
        {
            AssertEqBits();
        }
    }
}