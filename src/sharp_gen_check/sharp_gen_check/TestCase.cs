using System.IO;

namespace sharp_gen_check
{
    public abstract class TestCase
    {
        protected TestCase(decimal varA, decimal varB)
        {
            VarA = varA;
            VarB = varB;
            ResReturn = 0;
        }

        protected const string NameVarA = "var_a";
        protected const string NameVarB = "var_b";
        protected const string NameRes = "res";
        protected const string NameResReturn = "return_res";


        protected decimal VarA { get; set; }
        protected decimal VarB { get; set; }
        protected int[] BitsRes { get; set; } = null!;
        protected decimal Res = 0m;
        protected int ResReturn_custom { get; set; }
        protected int ResReturn { get; set; }
        protected StreamWriter File = null!;


        private static uint _testNum;
        private static string _testName = "def";


        protected static uint TestNum => _testNum++;

        protected string TestName
        {
            get => _testName;
            set => _testName = value;
        }


        protected abstract void WriteVar();
        protected abstract void WriteFunk();

        protected abstract void WriteAssert();

        protected virtual void InitTest()
        {
            File.WriteLine("#test {0}_{1}", TestName, TestNum);
        }

        protected void FileWriteVar(decimal variable, string nameVar)
        {
            var tmp = decimal.GetBits(variable);

            File.WriteLine("\t decimal_t {4} = {{ {0}, {1}, {2}, {3} }};", tmp[0],
                tmp[1], tmp[2], tmp[3], nameVar);
            File.WriteLine("\t\t// var: {0}", variable);
        }

        protected void AssertEqBits()
        {
            if (ResReturn == 0)
            {
                File.WriteLine("\t\t//var: {0}", new  decimal(BitsRes));
                for (var i = 0; i < 4; i++)
                {
                    File.WriteLine("\t ck_assert_int_eq({0}.bits[{2}], {1});", NameRes, BitsRes[i], i);
                }
                
            }
            File.WriteLine("\t ck_assert_int_eq({0}, {1});", NameResReturn, ResReturn);
            
        }

        protected void WriteInitRes()
        {
            File.WriteLine("\t decimal_t {0};", NameRes);
            File.WriteLine("\t nulling_decimal(&{0});", NameRes);
        }

        private void WriteStartCheck()
        {
            if (new FileInfo($"./tests/{TestName}.check").Length != 0) return;
            File.WriteLine("#include \"decimal.h\"");
            File.WriteLine("#include \"decimal__extra.h\"");
            File.WriteLine("");
        }

        public void RunWriteFile()
        {
            File = new StreamWriter($"./tests/{TestName}.check", true);
            WriteStartCheck();
            InitTest();
            WriteVar();
            WriteFunk();
            WriteAssert();
            File.WriteLine("");
            File.Close();
        }
    }
}