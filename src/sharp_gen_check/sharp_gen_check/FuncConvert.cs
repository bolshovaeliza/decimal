namespace sharp_gen_check;

public abstract class FuncConvert:TestCase
{
    protected int VarInt { get; set; }
    protected float VarFloat { get; set; }
    protected const string NameVarInt = "res_int";
    protected const string NameVarFloat = "res_float";
    protected int ResInt { get; set; }
    protected float ResFloat { get; set; }
    protected const string NameResInt = "res_int";
    protected const string NameResFloat = "res_float";
    protected FuncConvert(decimal varA) : base(varA, varB:0)
    {
    }
    
    protected override void InitTest()
    {
        base.InitTest();
        WriteInitRes();
    }

    protected override void WriteVar()
    {
    }
    

    protected override void WriteAssert()
    {
        AssertEqBits();
    }


}