namespace sharp_gen_check;

public class Mul : FuncArithmetic
{
    public Mul(decimal varA, decimal varB) : base(varA, varB)
    {
        TestName = "mul";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {3} = mul({0}, {1}, &{2});", NameVarA, NameVarB, NameRes, NameResReturn);
        try
        {
            BitsRes = decimal.GetBits(VarA * VarB);
        }
        catch (OverflowException)
        {
            if (decimal.Sign(VarA) == decimal.Sign(VarB))
            {
                ResReturn = 1;
                ResReturn_custom = 1;
            }
            else
            {
                ResReturn = 2;
                ResReturn_custom = 2;
            }
        }
    }
}