namespace sharp_gen_check;

public class IsNotEqual: FuncComp
{
    public IsNotEqual(decimal varA, decimal varB) : base(varA, varB)
    {
        TestName = "is_not_equal";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = is_not_equal({0}, {1});", NameVarA, NameVarB, NameResReturn);
        ResReturn = Convert.ToInt32(VarA != VarB);
        ResReturn_custom = ResReturn;
    }
}