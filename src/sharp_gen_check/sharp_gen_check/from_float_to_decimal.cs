namespace sharp_gen_check;

public class FromFloatToDecimal: FuncConvert
{
    public FromFloatToDecimal(float varFloat) : base(varA:0)
    {
        TestName = "from_float_to_decimal";
        VarFloat = varFloat;
    }

    protected override void WriteVar()
    {
       base.WriteVar();
       File.WriteLine("\t float {0} = {1};", NameVarFloat, VarFloat.ToString(System.Globalization.CultureInfo.InvariantCulture));
    }
    
    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = from_float_to_decimal({0}, &{1});", NameVarFloat, NameRes, NameResReturn);
        try
        {
            BitsRes = decimal.GetBits(Convert.ToDecimal(VarFloat));
        }
        catch (OverflowException )
        {
            ResReturn = 1;
            ResReturn_custom = 1;
        }
   
    }
}