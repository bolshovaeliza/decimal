namespace sharp_gen_check;

public class Mod: FuncArithmetic
{
    public Mod(decimal varA, decimal varB) : base(varA, varB)
    {
        TestName = "mod";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {3} = mod({0}, {1}, &{2});", NameVarA, NameVarB, NameRes, NameResReturn);
        try
        {
            BitsRes = decimal.GetBits(VarA % VarB);
        }
        catch (OverflowException)
        {
            if (decimal.Sign(VarA) == decimal.Sign(VarB))
            {
                ResReturn = 1;
                ResReturn_custom = 1;
            }
            else
            {
                ResReturn = 2;
                ResReturn_custom = 2;
            }
        }
        catch (DivideByZeroException)
        {
            ResReturn = 3;
            ResReturn_custom = 3;
        }
    }
}