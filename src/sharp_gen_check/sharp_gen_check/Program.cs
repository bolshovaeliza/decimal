﻿// using System;
// using System.IO;

using System.Text.RegularExpressions;

namespace sharp_gen_check
{
    internal static class Program
    {
        private static void DeleteOldCheck()
        {
            var regex = new Regex(@"^_.*.check");
            foreach (var file in new DirectoryInfo("./tests").GetFiles("*.*"))
            {
                if (!regex.IsMatch(file.Name))
                {
                    File.Delete(file.FullName);
                }
            }
        }

        private static decimal DecimalRnd(Boolean loR, Boolean midR, Boolean hiR, Boolean signR, Boolean expR )
        {
            var rnd = new Random();
            var lo = rnd.Next(Int32.MinValue, Int32.MaxValue);
            var mid = rnd.Next(Int32.MinValue, Int32.MaxValue);
            var hi = rnd.Next(Int32.MinValue, Int32.MaxValue);
            var sign = Convert.ToBoolean(rnd.Next(0, 2));
            var exp = Convert.ToByte(rnd.Next(0,28));
            return new decimal(loR?lo:0, midR?mid:0, hiR?hi:0, signR?sign:Convert.ToBoolean(0), expR?exp:Convert.ToByte(0));
        }
        // ==================================================================
        // arithmetic
        // ==================================================================

        private static void TestAdd()
        {
            new Add(7.1234m, 895.3m).RunWriteFile();
            new Add(744345.1234m, 0.003m).RunWriteFile();
            new Add(744345.1234m, 1234567890123456789012345678m).RunWriteFile();
            new Add(decimal.MaxValue, 1m).RunWriteFile();
            new Add(decimal.MinValue, -1m).RunWriteFile();
            new Add(7652369386474.2717959469530238m, 3803690521331.0142445561535780m).RunWriteFile();
            new Add(79228162514.264337593543950335m, 1m).RunWriteFile();
            new Add(79228162514.264337593543950335m, 79228162514.264337593543950335m).RunWriteFile();
            new Add(7.9228162514264337593543950335m, 7.9228162514264337593543950335m).RunWriteFile();
            new Add(7.9228162514264337593543950335m, 1m).RunWriteFile();
            new Add(7.9228162514264337593543950336m, 1.01m).RunWriteFile();
            new Add(787m, 895m).RunWriteFile();
            new Add(784m, 89m).RunWriteFile();
            new Add(7856435642134m, 895539482726474m).RunWriteFile();
            new Add(4294967296m, 21474836480m).RunWriteFile();
            new Add(42949672.96m, 214748364.80m).RunWriteFile();
            new Add(3000000000m, 6000000000m).RunWriteFile();

            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new Add(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,false,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new Add(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,false,true,true);
                new Add(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,false,true,true);
                var tmp2 = DecimalRnd(true, true,false,true,true);
                new Add(tmp1, tmp2).RunWriteFile();
            }
        }

        private static void TestSub()
        {
            new Sub(5880m, 855m).RunWriteFile();
            new Sub(79228162514264337593543950335m, -8557565564345m).RunWriteFile();
            new Sub(1.98789m, 1m).RunWriteFile();
            new Sub(23523m, 3423m).RunWriteFile();
            new Sub(235m, 34m).RunWriteFile();
            new Sub(2352563672346733m, 342345747m).RunWriteFile();
            new Sub(235256367239346733m, 342345547747m).RunWriteFile();
            new Sub(235256367523467378768333m, 9442345345345747m).RunWriteFile();
            new Sub(2352356367523467378768333m, 92442345345345747m).RunWriteFile();

            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new Sub(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,false,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new Sub(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,false,true,true);
                new Sub(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,false,true,true);
                var tmp2 = DecimalRnd(true, true,false,true,true);
                new Sub(tmp1, tmp2).RunWriteFile();
            }
        }

        private static void TestMul()
        {
            new Mul(123m, 32m).RunWriteFile();
                new Mul(126m, 78m).RunWriteFile();
            new Mul(0m, 1m).RunWriteFile();
            new Mul(1m, 0m).RunWriteFile();
            new Mul(678652873687340982030m, 0.00000000000001m).RunWriteFile();
            new Mul(0.00000000000001m, 678652873687340982030m).RunWriteFile();
            new Mul(1m, 13m).RunWriteFile();
            new Mul(1m, 3m).RunWriteFile();
            new Mul(13m, 7m).RunWriteFile();

            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new Mul(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,false,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new Mul(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,false,true,true);
                new Mul(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,false,true,true);
                var tmp2 = DecimalRnd(true, true,false,true,true);
            }
        }

        private static void TestDiv()
        {
            new Div(126m, 78m).RunWriteFile();
            new Div(0m, 1m).RunWriteFile();
            new Div(1m, 0m).RunWriteFile();
            new Div(678652873687340982030m, 0.00000000000001m).RunWriteFile();
            new Div(0.00000000000001m, 678652873687340982030m).RunWriteFile();
            new Div(1m, 13m).RunWriteFile();
            new Div(1m, 3m).RunWriteFile();
            new Div(13m, 7m).RunWriteFile();
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new Div(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,false,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new Div(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,false,true,true);
                new Div(tmp1, tmp2).RunWriteFile();
            }
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,false,true,true);
                var tmp2 = DecimalRnd(true, true,false,true,true);
                new Div(tmp1, tmp2).RunWriteFile();
            }
        }

        private static void TestMod()
        {
            new Mod(1245m, 0m).RunWriteFile();
            new Mod(3m, 2m).RunWriteFile();
            new Mod(4m, 2m).RunWriteFile();
            new Mod(1m, 2m).RunWriteFile();
            new Mod(10m, 3m).RunWriteFile();
            new Mod(11m, 6m).RunWriteFile();
            new Mod(11m, 7m).RunWriteFile();
            new Mod(1m, 0.00000001m).RunWriteFile();
            new Mod(1m, 0.00000002m).RunWriteFile();
            new Mod(1m, 0.00000021m).RunWriteFile();
            new Mod(1m, 0.00000022m).RunWriteFile();
            new Mod(1m, 0.00000031m).RunWriteFile();
            new Mod(1m, 0.00003001m).RunWriteFile();
            new Mod(3m, 2m).RunWriteFile();
            new Mod(3.7443m, 2m).RunWriteFile();

            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,false,true,false);
                var tmp2 = DecimalRnd(true, true,false,true,false);
                new Mod(tmp1, tmp2).RunWriteFile();
            }
          
        }

        // ==================================================================
        // compare
        // ==================================================================

        private static void TestLess()
        {
            new IsLess(1m, 2m).RunWriteFile();
            new IsLess(123456789123m, 1234567.89123m).RunWriteFile();
            new IsLess(-123456789123m, 1234567.89123m).RunWriteFile();
            new IsLess(123456789123m, 123456789123m).RunWriteFile();
            new IsLess(-123456789123m, 123456789123m).RunWriteFile();
            new IsLess(1234567.89123m, 123456789123m).RunWriteFile();
            new IsLess(1234567.89123m, -123456789123m).RunWriteFile();
            new IsLess(0.123456789123m, 123456789123m).RunWriteFile();
            new IsLess(123456789123m, -123456789123m).RunWriteFile();
            new IsLess(0.000000m, -0.000000m).RunWriteFile();
            new IsLess(0.000000m, 0.000000m).RunWriteFile();
            new IsLess(-0.000000m, 0.000000m).RunWriteFile();
            new IsLess(-0.000000m, -0.000000m).RunWriteFile();
            new IsLess(0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsLess(-0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsLess(0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsLess(0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsLess(-0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsLess(0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsLess(-0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsLess(-0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsLess(0.000000000016m, 0.00000000001m).RunWriteFile();
            new IsLess(0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsLess(0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsLess(-0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsLess(-0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsLess(0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsLess(-0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsLess(0.00000000001m, -0.000000000011m).RunWriteFile();
            new IsLess(-0.00000000001m, -0.000000000011m).RunWriteFile();
            new IsLess(3m, 2m).RunWriteFile();
            new IsLess(1m, 0.1m).RunWriteFile();
            new IsLess(0.1m, 1m).RunWriteFile();
            new IsLess(1m, 1.0m).RunWriteFile();
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new IsLess(tmp1, tmp2).RunWriteFile();
            }
        }

        private static void TestLessEq()
        {
            new IsLessOrEqual(1m, 3m).RunWriteFile();
            new IsLessOrEqual(3m, 2m).RunWriteFile();
            new IsLessOrEqual(1m, 0.1m).RunWriteFile();
            new IsLessOrEqual(0.1m, 1m).RunWriteFile();
            new IsLessOrEqual(1m, 1.0m).RunWriteFile();
            new IsLessOrEqual(123456789123m, 1234567.89123m).RunWriteFile();
            new IsLessOrEqual(-123456789123m, 1234567.89123m).RunWriteFile();
            new IsLessOrEqual(123456789123m, 123456789123m).RunWriteFile();
            new IsLessOrEqual(-123456789123m, 123456789123m).RunWriteFile();
            new IsLessOrEqual(1234567.89123m, 123456789123m).RunWriteFile();
            new IsLessOrEqual(1234567.89123m, -123456789123m).RunWriteFile();
            new IsLessOrEqual(0.123456789123m, 123456789123m).RunWriteFile();
            new IsLessOrEqual(123456789123m, -123456789123m).RunWriteFile();
            new IsLessOrEqual(0.000000m, -0.000000m).RunWriteFile();
            new IsLessOrEqual(0.000000m, 0.000000m).RunWriteFile();
            new IsLessOrEqual(-0.000000m, 0.000000m).RunWriteFile();
            new IsLessOrEqual(-0.000000m, -0.000000m).RunWriteFile();
            new IsLessOrEqual(0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsLessOrEqual(-0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsLessOrEqual(0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsLessOrEqual(0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsLessOrEqual(-0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsLessOrEqual(0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsLessOrEqual(-0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsLessOrEqual(-0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsLessOrEqual(0.000000000016m, 0.00000000001m).RunWriteFile();
            new IsLessOrEqual(0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsLessOrEqual(0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsLessOrEqual(-0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsLessOrEqual(-0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsLessOrEqual(0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsLessOrEqual(-0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsLessOrEqual(0.00000000001m, -0.000000000011m).RunWriteFile();
            new IsLessOrEqual(-0.00000000001m, -0.000000000011m).RunWriteFile();
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new IsLessOrEqual(tmp1, tmp2).RunWriteFile();
            }
        }

        private static void TestGreater()
        {
            new IsGreater(1m, 2m).RunWriteFile();
            new IsGreater(3m, 2m).RunWriteFile();
            new IsGreater(1m, 0.1m).RunWriteFile();
            new IsGreater(0.1m, 1m).RunWriteFile();
            new IsGreater(1m, 1.0m).RunWriteFile();
            new IsGreater(123456789123m, 1234567.89123m).RunWriteFile();
            new IsGreater(-123456789123m, 1234567.89123m).RunWriteFile();
            new IsGreater(123456789123m, 123456789123m).RunWriteFile();
            new IsGreater(-123456789123m, 123456789123m).RunWriteFile();
            new IsGreater(1234567.89123m, 123456789123m).RunWriteFile();
            new IsGreater(1234567.89123m, -123456789123m).RunWriteFile();
            new IsGreater(0.123456789123m, 123456789123m).RunWriteFile();
            new IsGreater(123456789123m, -123456789123m).RunWriteFile();
            new IsGreater(0.000000m, -0.000000m).RunWriteFile();
            new IsGreater(0.000000m, 0.000000m).RunWriteFile();
            new IsGreater(-0.000000m, 0.000000m).RunWriteFile();
            new IsGreater(-0.000000m, -0.000000m).RunWriteFile();
            new IsGreater(0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsGreater(-0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsGreater(0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsGreater(0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsGreater(-0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsGreater(0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsGreater(-0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsGreater(-0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsGreater(0.000000000016m, 0.00000000001m).RunWriteFile();
            new IsGreater(0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsGreater(0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsGreater(-0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsGreater(-0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsGreater(0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsGreater(-0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsGreater(0.00000000001m, -0.000000000011m).RunWriteFile();
            new IsGreater(-0.00000000001m, -0.000000000011m).RunWriteFile();
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new IsGreater(tmp1, tmp2).RunWriteFile();
            }
        }

        private static void TestGreaterEq()
        {
            new IsGreaterOrEqual(1m, 2m).RunWriteFile();
            new IsGreaterOrEqual(3m, 2m).RunWriteFile();
            new IsGreaterOrEqual(1m, 0.1m).RunWriteFile();
            new IsGreaterOrEqual(0.1m, 1m).RunWriteFile();
            new IsGreaterOrEqual(1m, 1.0m).RunWriteFile();
            new IsGreaterOrEqual(123456789123m, 1234567.89123m).RunWriteFile();
            new IsGreaterOrEqual(-123456789123m, 1234567.89123m).RunWriteFile();
            new IsGreaterOrEqual(123456789123m, 123456789123m).RunWriteFile();
            new IsGreaterOrEqual(-123456789123m, 123456789123m).RunWriteFile();
            new IsGreaterOrEqual(1234567.89123m, 123456789123m).RunWriteFile();
            new IsGreaterOrEqual(1234567.89123m, -123456789123m).RunWriteFile();
            new IsGreaterOrEqual(0.123456789123m, 123456789123m).RunWriteFile();
            new IsGreaterOrEqual(123456789123m, -123456789123m).RunWriteFile();
            new IsGreaterOrEqual(0.000000m, -0.000000m).RunWriteFile();
            new IsGreaterOrEqual(0.000000m, 0.000000m).RunWriteFile();
            new IsGreaterOrEqual(-0.000000m, 0.000000m).RunWriteFile();
            new IsGreaterOrEqual(-0.000000m, -0.000000m).RunWriteFile();
            new IsGreaterOrEqual(0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsGreaterOrEqual(-0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsGreaterOrEqual(0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsGreaterOrEqual(0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsGreaterOrEqual(-0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsGreaterOrEqual(0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsGreaterOrEqual(-0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsGreaterOrEqual(-0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsGreaterOrEqual(0.000000000016m, 0.00000000001m).RunWriteFile();
            new IsGreaterOrEqual(0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsGreaterOrEqual(0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsGreaterOrEqual(-0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsGreaterOrEqual(-0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsGreaterOrEqual(0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsGreaterOrEqual(-0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsGreaterOrEqual(0.00000000001m, -0.000000000011m).RunWriteFile();
            new IsGreaterOrEqual(-0.00000000001m, -0.000000000011m).RunWriteFile();
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new IsGreaterOrEqual(tmp1, tmp2).RunWriteFile();
            }
        }

        private static void TestEq()
        {
            new IsEqual(1m, 2m).RunWriteFile();
            new IsEqual(3m, 2m).RunWriteFile();
            new IsEqual(1m, 0.1m).RunWriteFile();
            new IsEqual(0.1m, 1m).RunWriteFile();
            new IsEqual(1m, 1.0m).RunWriteFile();
            new IsEqual(123456789123m, 1234567.89123m).RunWriteFile();
            new IsEqual(-123456789123m, 1234567.89123m).RunWriteFile();
            new IsEqual(123456789123m, 123456789123m).RunWriteFile();
            new IsEqual(-123456789123m, 123456789123m).RunWriteFile();
            new IsEqual(1234567.89123m, 123456789123m).RunWriteFile();
            new IsEqual(1234567.89123m, -123456789123m).RunWriteFile();
            new IsEqual(0.123456789123m, 123456789123m).RunWriteFile();
            new IsEqual(123456789123m, -123456789123m).RunWriteFile();
            new IsEqual(0.000000m, -0.000000m).RunWriteFile();
            new IsEqual(0.000000m, 0.000000m).RunWriteFile();
            new IsEqual(-0.000000m, 0.000000m).RunWriteFile();
            new IsEqual(-0.000000m, -0.000000m).RunWriteFile();
            new IsEqual(0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsEqual(-0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsEqual(0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsEqual(0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsEqual(-0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsEqual(0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsEqual(-0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsEqual(-0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsEqual(0.000000000016m, 0.00000000001m).RunWriteFile();
            new IsEqual(0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsEqual(0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsEqual(-0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsEqual(-0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsEqual(0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsEqual(-0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsEqual(0.00000000001m, -0.000000000011m).RunWriteFile();
            new IsEqual(-0.00000000001m, -0.000000000011m).RunWriteFile();
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new IsEqual(tmp1, tmp2).RunWriteFile();
            }
        }

        private static void TestNotEq()
        {
            new IsNotEqual(1m, 2m).RunWriteFile();
            new IsNotEqual(3m, 2m).RunWriteFile();
            new IsNotEqual(1m, 0.1m).RunWriteFile();
            new IsNotEqual(0.1m, 1m).RunWriteFile();
            new IsNotEqual(1m, 1.0m).RunWriteFile();
            new IsNotEqual(123456789123m, 1234567.89123m).RunWriteFile();
            new IsNotEqual(-123456789123m, 1234567.89123m).RunWriteFile();
            new IsNotEqual(123456789123m, 123456789123m).RunWriteFile();
            new IsNotEqual(-123456789123m, 123456789123m).RunWriteFile();
            new IsNotEqual(1234567.89123m, 123456789123m).RunWriteFile();
            new IsNotEqual(1234567.89123m, -123456789123m).RunWriteFile();
            new IsNotEqual(0.123456789123m, 123456789123m).RunWriteFile();
            new IsNotEqual(123456789123m, -123456789123m).RunWriteFile();
            new IsNotEqual(0.000000m, -0.000000m).RunWriteFile();
            new IsNotEqual(0.000000m, 0.000000m).RunWriteFile();
            new IsNotEqual(-0.000000m, 0.000000m).RunWriteFile();
            new IsNotEqual(-0.000000m, -0.000000m).RunWriteFile();
            new IsNotEqual(0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsNotEqual(-0.00000000001m, 0.00000000002m).RunWriteFile();
            new IsNotEqual(0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsNotEqual(0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsNotEqual(-0.00000000002m, 0.00000000001m).RunWriteFile();
            new IsNotEqual(0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsNotEqual(-0.00000000002m, -0.00000000001m).RunWriteFile();
            new IsNotEqual(-0.00000000001m, -0.00000000002m).RunWriteFile();
            new IsNotEqual(0.000000000016m, 0.00000000001m).RunWriteFile();
            new IsNotEqual(0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsNotEqual(0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsNotEqual(-0.000000000011m, 0.00000000001m).RunWriteFile();
            new IsNotEqual(-0.000000000011m, -0.00000000001m).RunWriteFile();
            new IsNotEqual(0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsNotEqual(-0.00000000001m, 0.000000000011m).RunWriteFile();
            new IsNotEqual(0.00000000001m, -0.000000000011m).RunWriteFile();
            new IsNotEqual(-0.00000000001m, -0.000000000011m).RunWriteFile();
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true,true,true,true);
                var tmp2 = DecimalRnd(true, true,true,true,true);
                new IsNotEqual(tmp1, tmp2).RunWriteFile();
            }
        }

        // ==================================================================
        // convert
        // ==================================================================

        private static void TestConvertFromInt()
        {
            new FromIntToDecimal(34452).RunWriteFile();
            new FromIntToDecimal(-34452).RunWriteFile();
            new FromIntToDecimal(-1).RunWriteFile();
            new FromIntToDecimal(0).RunWriteFile();
            new FromIntToDecimal(-0).RunWriteFile();
            new FromIntToDecimal(2147483647).RunWriteFile();
            new FromIntToDecimal(-2147483647).RunWriteFile();
            for (int i = 0; i < 100; i++)
            {
                var rnd = new Random();
                var intVar = rnd.Next(Int32.MinValue, Int32.MaxValue);
                new FromIntToDecimal(intVar).RunWriteFile();
            }
        }

        private static void TestConvertFromFloat()
        {
            var rnd = new Random();
            const double range = (double)float.MaxValue - (double)float.MinValue;
            new FromFloatToDecimal(4562.25f).RunWriteFile();
            new FromFloatToDecimal(4562.25f).RunWriteFile();
            new FromFloatToDecimal(0f).RunWriteFile();
            new FromFloatToDecimal(-0f).RunWriteFile();
            new FromFloatToDecimal(1f).RunWriteFile();
            new FromFloatToDecimal(-1f).RunWriteFile();
            new FromFloatToDecimal(1.0000000f).RunWriteFile();
            new FromFloatToDecimal(-1.0000000f).RunWriteFile();
            new FromFloatToDecimal(789.887f).RunWriteFile();
            new FromFloatToDecimal(-72123.2554f).RunWriteFile();
            new FromFloatToDecimal(-3.402823466E+38f).RunWriteFile();
            new FromFloatToDecimal(-7.92281625143e+28f).RunWriteFile();
            new FromFloatToDecimal(7.92281625143e+28f).RunWriteFile();
            new FromFloatToDecimal(7.92281577919e+28f).RunWriteFile();
            new FromFloatToDecimal(-7.92281577919e+28f).RunWriteFile();
            for (var i = 0; i < 100; i++)
            {
                var sample = rnd.NextDouble();
                var scaled = (sample * range) + float.MinValue;
                new FromFloatToDecimal((float)scaled).RunWriteFile();
            }
        }

        private static void TestConvertFromDecToFloat()
        {
            new FromDecimalToFloat(456584123215484651m).RunWriteFile();
            new FromDecimalToFloat(new decimal(-1,-1,0,false,0)).RunWriteFile();
            new FromDecimalToFloat(new decimal(2,-1,0,false,0)).RunWriteFile();
            new FromDecimalToFloat(new decimal(0,-1,0,false,0)).RunWriteFile();
            new FromDecimalToFloat(new decimal(0,-1,0,false,1)).RunWriteFile();
            new FromDecimalToFloat(79228162514264337593543950335m).RunWriteFile();
            new FromDecimalToFloat(79228162514264337593543950334m).RunWriteFile();
            new FromDecimalToFloat(79228162514264337593543950330m).RunWriteFile();
            new FromDecimalToFloat(79228162514264337593543950329m).RunWriteFile();
            new FromDecimalToFloat(0.000000000000000000000000001m).RunWriteFile();
            new FromDecimalToFloat(7922816251426433759354395033m).RunWriteFile();
            new FromDecimalToFloat(7922816251426433759354395033.5m).RunWriteFile();
            new FromDecimalToFloat(0.5m).RunWriteFile();
            new FromDecimalToFloat(7922816.2514264337593543950335m).RunWriteFile();
            new FromDecimalToFloat(0.000000000000000032000023m).RunWriteFile();
            new FromDecimalToFloat(100000000000m).RunWriteFile();
            new FromDecimalToFloat(10000000000000.00000003m).RunWriteFile();
            new FromDecimalToFloat(0m).RunWriteFile();
            new FromDecimalToFloat(0.1m).RunWriteFile();
            new FromDecimalToFloat(0.11m).RunWriteFile();
            new FromDecimalToFloat(0.15m).RunWriteFile();
            new FromDecimalToFloat(0.19m).RunWriteFile();
            new FromDecimalToFloat(0.2m).RunWriteFile();
            new FromDecimalToFloat(0.25m).RunWriteFile();
            new FromDecimalToFloat(0.29m).RunWriteFile();
            new FromDecimalToFloat(0.01m).RunWriteFile();
            new FromDecimalToFloat(0.013m).RunWriteFile();
            new FromDecimalToFloat(0.015m).RunWriteFile();
            new FromDecimalToFloat(0.019m).RunWriteFile();
            new FromDecimalToFloat(263.3m).RunWriteFile();
            new FromDecimalToFloat(45.45m).RunWriteFile();
            new FromDecimalToFloat(1.123215484651m).RunWriteFile();
            new FromDecimalToFloat(1.3m).RunWriteFile();
            new FromDecimalToFloat(0.03m).RunWriteFile();
            new FromDecimalToFloat(-10.3m).RunWriteFile();
            new FromDecimalToFloat(-0.03m).RunWriteFile();
            new FromDecimalToFloat(1m).RunWriteFile();
            new FromDecimalToFloat(456584.123215484651m).RunWriteFile();
            new FromDecimalToFloat(100m).RunWriteFile();
             for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true, true, true,true);
                new FromDecimalToFloat(tmp1).RunWriteFile();
            }
        }
        private static void TestConvertFromDecToInt()
        {
            new FromDecimalToInt(456484651m).RunWriteFile();
            new FromDecimalToInt(4564843786387638745651m).RunWriteFile();
            new FromDecimalToInt(0.9m).RunWriteFile();
            new FromDecimalToInt(1m).RunWriteFile();
            new FromDecimalToInt(456584.123215484651m).RunWriteFile();
            new FromDecimalToInt(456584.999915484651m).RunWriteFile();
            new FromDecimalToInt(100m).RunWriteFile();
            new FromDecimalToInt(-456484651m).RunWriteFile();
            new FromDecimalToInt(-4564843786387638745651m).RunWriteFile();
            new FromDecimalToInt(-0.9m).RunWriteFile();
            new FromDecimalToInt(-1m).RunWriteFile();
            new FromDecimalToInt(-456584.123215484651m).RunWriteFile();
            new FromDecimalToInt(-456584.999915484651m).RunWriteFile();
            new FromDecimalToInt(-100m).RunWriteFile();
            new FromDecimalToInt(0m).RunWriteFile();
            new FromDecimalToInt(-0m).RunWriteFile();
            new FromDecimalToInt(0.3m).RunWriteFile();
            new FromDecimalToInt(-0.3m).RunWriteFile();
            new FromDecimalToInt(1.3m).RunWriteFile();
            new FromDecimalToInt(-1.3m).RunWriteFile();
            new FromDecimalToInt(2.3m).RunWriteFile();
            new FromDecimalToInt(-2.3m).RunWriteFile();
            new FromDecimalToInt(new decimal(-1,0,0, false, 0)).RunWriteFile();
            new FromDecimalToInt(new decimal(-1,0,0, true, 0)).RunWriteFile();
            for (int i = 0; i < 100; i++)
            {
                var tmp1 = DecimalRnd(true, true, true, true,true);
                new FromDecimalToInt(tmp1).RunWriteFile();
            }
        }


        // ==================================================================
        // other
        // ==================================================================

        private static void TestFloor()
        {
            new Floor(123.3m).RunWriteFile();
            new Floor(-1.3m).RunWriteFile();
            new Floor(-1.5m).RunWriteFile();
            new Floor(-1.6m).RunWriteFile();
            new Floor(-2.5m).RunWriteFile();
            new Floor(-2.501m).RunWriteFile();
            new Floor(-1.501m).RunWriteFile();
            new Floor(1.3m).RunWriteFile();
            new Floor(1.5m).RunWriteFile();
            new Floor(1.6m).RunWriteFile();
            new Floor(2.5m).RunWriteFile();
            new Floor(2.501m).RunWriteFile();
            new Floor(1.501m).RunWriteFile();
            new Floor(123.00000000000000003m).RunWriteFile();
            new Floor(-1.000000000000000003m).RunWriteFile();
            new Floor(-1.000000000000000005m).RunWriteFile();
            new Floor(-1.000000000000000006m).RunWriteFile();
            new Floor(-2.000000000000000005m).RunWriteFile();
            new Floor(-2.00000000000000000501m).RunWriteFile();
            new Floor(-1.00000000000000000501m).RunWriteFile();
            new Floor(1.0000000000000000003m).RunWriteFile();
            new Floor(1.0000000000000000005m).RunWriteFile();
            new Floor(1.0000000000000000006m).RunWriteFile();
            new Floor(2.0000000000000000005m).RunWriteFile();
            new Floor(2.000000000000000000501m).RunWriteFile();
            new Floor(1.000000000000000000501m).RunWriteFile();
            for (var i = 1; i < 100; i++)
            {
                new Floor(DecimalRnd(true, true, true,true,true)).RunWriteFile();
            }
        }

        private static void TestRound()
        {
            new Round(123.3m).RunWriteFile();
            new Round(123.5m).RunWriteFile();
            new Round(122.5m).RunWriteFile();
            new Round(123.51m).RunWriteFile();
            new Round(122.51m).RunWriteFile();
            new Round(-1.3m).RunWriteFile();
            new Round(-1.5m).RunWriteFile();
            new Round(-1.6m).RunWriteFile();
            new Round(-2.5m).RunWriteFile();
            new Round(-2.501m).RunWriteFile();
            new Round(-1.501m).RunWriteFile();
            new Round(1.3m).RunWriteFile();
            new Round(1.5m).RunWriteFile();
            new Round(1.6m).RunWriteFile();
            new Round(2.5m).RunWriteFile();
            new Round(2.501m).RunWriteFile();
            new Round(1.501m).RunWriteFile();
            for (var i = 1; i < 100; i++)
            {
                new Round(DecimalRnd(true, true, true,true,true)).RunWriteFile();
            }
        }

        private static void TestTruncate()
        {
            new Truncate(0.9m).RunWriteFile();
            new Truncate(0.000000000000m).RunWriteFile();
            new Truncate(-0.000000000000m).RunWriteFile();
            new Truncate(0.00000000000004m).RunWriteFile();
            new Truncate(-0.00000000000004m).RunWriteFile();
            new Truncate(100.000000000000m).RunWriteFile();
            new Truncate(-100.000000000000m).RunWriteFile();
            new Truncate(11.000000000000m).RunWriteFile();
            new Truncate(-11.000000000000m).RunWriteFile();
            new Truncate(000.000000000000m).RunWriteFile();
            new Truncate(-0000.000000000000m).RunWriteFile();
            new Truncate(010.000000000000m).RunWriteFile();
            new Truncate(-010.000000000000m).RunWriteFile();
            new Truncate(-1.5m).RunWriteFile();
            new Truncate(-1.6m).RunWriteFile();
            new Truncate(-1.3m).RunWriteFile();
            new Truncate(-2.5m).RunWriteFile();
            new Truncate(-2.501m).RunWriteFile();
            new Truncate(-1.501m).RunWriteFile();
            new Truncate(1.3m).RunWriteFile();
            new Truncate(1.5m).RunWriteFile();
            new Truncate(1.6m).RunWriteFile();
            new Truncate(2.5m).RunWriteFile();
            new Truncate(2.501m).RunWriteFile();
            new Truncate(1.501m).RunWriteFile();
            for (var i = 1; i < 15; i++)
            {
                new Truncate(84562327643451.85212m / (i * 1.2349m)).RunWriteFile();
            }

            for (var i = 1; i < 10; i++)
            {
                new Truncate(-0.8335212m / i).RunWriteFile();
            }
            for (var i = 1; i < 100; i++)
            {
                new Truncate(DecimalRnd(true, true, true,true,true)).RunWriteFile();
            }
        }

        private static void TestNegate()
        {
            new Negate(888545m).RunWriteFile();
            new Negate(88565775648545m).RunWriteFile();
            new Negate(-1m).RunWriteFile();
            new Negate(1m).RunWriteFile();
            new Negate(-1.3m).RunWriteFile();
            new Negate(-1.5m).RunWriteFile();
            new Negate(-1.6m).RunWriteFile();
            new Negate(-2.5m).RunWriteFile();
            new Negate(-2.501m).RunWriteFile();
            new Negate(-1.501m).RunWriteFile();
            new Negate(1.3m).RunWriteFile();
            new Negate(1.5m).RunWriteFile();
            new Negate(1.6m).RunWriteFile();
            new Negate(2.5m).RunWriteFile();
            new Negate(2.501m).RunWriteFile();
            new Negate(1.501m).RunWriteFile();
        }

        private static void TestStepRound()
        {
            new RoundStep(1234.56m).RunWriteFile();
            new RoundStep(1234.565m).RunWriteFile();
            new RoundStep(1234.555m).RunWriteFile();
            new RoundStep(1234.5m).RunWriteFile();
            new RoundStep(1233.5m).RunWriteFile();
            
            var decA = 1.0123456789m;
            for (int i = 0; i < 10; i++)
            {
                var bitsDecA = decimal.GetBits(decA );
                var expDecA = (bitsDecA[3] & 0xff0000) >> 16;
                new RoundStep(decA).RunWriteFile();
                decA = decimal.Round(decA, (expDecA >0? expDecA : 1)-1, MidpointRounding.ToEven);
            }
        }

        public static void Main(string[] args)
        {
            DeleteOldCheck();
            // arithmetics
            TestAdd();
            TestSub();
            TestMul();
            TestDiv();
            TestMod();

            // compare
            TestLess();
            TestLessEq();
            TestGreater();
            TestGreaterEq();
            TestEq();
            TestNotEq();

            //convert
            TestConvertFromInt();   
            TestConvertFromFloat();
            TestConvertFromDecToInt();
            TestConvertFromDecToFloat();
        
            // other
            TestFloor();
            TestRound();
            TestNegate();
            TestTruncate();
            
            TestStepRound();

            Console.WriteLine("tests created!");
        }
    }
}