namespace sharp_gen_check;

public class IsGreaterOrEqual: FuncComp
{
    public IsGreaterOrEqual(decimal varA, decimal varB) : base(varA, varB)
    {
        TestName = "is_greater_or_equal";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = is_greater_or_equal({0}, {1});", NameVarA, NameVarB, NameResReturn);
        ResReturn = Convert.ToInt32(VarA >= VarB);
        ResReturn = ResReturn;    }
}