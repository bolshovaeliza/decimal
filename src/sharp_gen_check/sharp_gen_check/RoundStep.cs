namespace sharp_gen_check;

public class RoundStep: FuncOther
{
    public RoundStep(decimal varA) : base(varA)
    {
        TestName = "round_step";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t round_struct tmp = {0};");
        File.WriteLine("\t bank_round(&{0}, &tmp);", NameVarA);
        File.WriteLine("\t equating_decimal({0}, &{1});", NameVarA, NameRes);
        File.WriteLine("\t int {0} = 0;", NameResReturn);
        
        BitsRes = decimal.GetBits(VarA );
        var expD = (BitsRes[3] & 0xff0000) >> 16;
        BitsRes = decimal.GetBits(decimal.Round(VarA, (expD>0? expD : 1)-1, MidpointRounding.ToEven));
    }
}