namespace sharp_gen_check;

public class Round: FuncOther
{
    public Round(decimal varA) : base(varA)
    {
        TestName = "round";
    }

    protected override void WriteFunk()
    {
        File.WriteLine("\t int {2} = decimal_round({0}, &{1});", NameVarA, NameRes, NameResReturn);
        BitsRes = decimal.GetBits(decimal.Round(VarA) );
    }
}