#include <math.h>

#include "decimal.h"
#include "decimal__extra.h"

void nulling_decimal(decimal_t *decimal) {
  if (decimal) *decimal = (decimal_t)(decimal_uint){0, 0, 0, 0, 0};
}

void equating_decimal(decimal_t src, decimal_t *dst) {
  if (dst) {
    for (int i = 0; i < 4; i++) dst->bits[i] = src.bits[i];
  }
}

int compare_sign(decimal_t a, decimal_t b) {
  return (a.bits_.sign == b.bits_.sign);
}

int compare_val(decimal_t a, decimal_t b) {
  int res = (a.bits_.exp == b.bits_.exp);
  if (res) {
    res = (a.bits[0] == b.bits[0]);
    res &= (a.bits[1] == b.bits[1]);
    res &= (a.bits[2] == b.bits[2]);
  }
  return res;
}

int is_empty(const decimal_t *val) {
  return !(val->bits[0] || val->bits[1] || val->bits[2]);
}

int is_empty_x4(const decimal_t *val) {
  return !(val->bits[0] || val->bits[1] || val->bits[2] || val->bits[3]);
}

int count_bits(unsigned int num, unsigned int base) {
  return (num == -1) ? 32
                     : ((int)ceil(log2((unsigned int)num + 1) / log2(base)));
}

int num_len(decimal_t a, unsigned int base) {
  int res = 0;
  if (base == 10) {
    while (a.bits_.byte_2 != 0 || a.bits_.byte_1 != 0 ||
           (unsigned int)a.bits_.byte_0 >= 10) {
      div_by_x(&a, 10);
      res++;
    }
    if (a.bits_.byte_2 == 0 && a.bits_.byte_1 == 0 && a.bits_.byte_0 < 10)
      res++;

  } else {
    if (a.bits[2]) {
      res += 64 + count_bits(a.bits[2], base);
    } else if (a.bits[1]) {
      res += 32 + count_bits(a.bits[1], base);
    } else if (a.bits[0]) {
      res = count_bits(a.bits[0], base);
    }
  }
  return res;
}

round_struct normalization(decimal_t *a, decimal_t *b) {
  int exp_dif = a->bits_.exp - b->bits_.exp;
  round_struct br = {0};
  decimal_t tmpdel, *temp;
  if (exp_dif < 0) {
    temp = a;
    a = b;
    b = temp;
    exp_dif *= -1;
    br.switched = 1;
  }
  while (exp_dif) {
    if ((a->bits_.exp > b->bits_.exp) && !multi_by_10(b)) {
      b->bits_.exp++;
      exp_dif--;
      br.m_scaled = 1;
    } else {
      bank_round(a, &br);
      exp_dif--;
      br.d_scaled = 1;
    }
  }
  br.b_less = !(br.switched) ? is_less_no_norm(*b, *a)
                             : is_less_no_norm(*a, *b);
  tmpdel = *b;
  br.norm_last_int = div_by_x(&tmpdel, 10);
  if (br.switched) {
    temp = a;
    a = b;
    b = temp;
  }
  return br;
}

int mantiss_cmp(decimal_t val1, decimal_t val2) {
  int res = 0;
  for (int i = 3; !res && i >= 0; i--) {
    res = (unsigned)val1.bits[i] >= (unsigned)val2.bits[i] &&
          (val1.bits[i] || val2.bits[i]);
    if (val1.bits[i] || val2.bits[i]) break;
  }

  return res;
}

int push_integer_x4(decimal_t *val, unsigned char point_pos) {
  int res = 0;
  multi_by_10_x4(val);
  res |= bit_get_x4(*val, point_pos + 4) << 3;
  res |= bit_get_x4(*val, point_pos + 3) << 2;
  res |= bit_get_x4(*val, point_pos + 2) << 1;
  res |= bit_get_x4(*val, point_pos + 1);
  decimal_t del = {~0, ~0, ~0, ~0};
  for (int i = 1; i <= point_pos + 1; i++) left_x4(&del);
  bit_and_x4(val, (decimal_t){~del.bits[0], ~del.bits[1], ~del.bits[2],
                                ~del.bits[3]});
  return res;
}
